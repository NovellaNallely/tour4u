
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { environment } from 'src/environments/environment';
import { FirebaseAuthService } from './Services/firebase-auth.service';
import { FirebaseDatabaseService } from './Services/firebase-database.service';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { GoogleMapsModule } from '@angular/google-maps';

import * as firebase from 'firebase/app';

//firebase.initializeApp(environment.firebase);
console.log("Inicializando firebase");
@NgModule({
  declarations: [
    AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    GoogleMapsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FirebaseAuthService,
    FirebaseDatabaseService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: FirebaseAuthService, useClass: FirebaseAuthService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
