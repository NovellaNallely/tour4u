import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TourListPage } from './tour-list.page';

describe('TourListPage', () => {
  let component: TourListPage;
  let fixture: ComponentFixture<TourListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TourListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
