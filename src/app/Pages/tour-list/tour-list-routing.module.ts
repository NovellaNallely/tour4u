import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TourListPage } from './tour-list.page';

const routes: Routes = [
  {
    path: '',
    component: TourListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TourListPageRoutingModule {}
