import { Component, OnInit } from '@angular/core';
import { FirebaseAuthService } from '../../Services/firebase-auth.service';
import { FirebaseDatabaseService } from '../../Services/firebase-database.service';
import { User } from 'src/app/models/user';
import { Tour } from 'src/app/models/tour';
import { MenuController } from '@ionic/angular';
import { Localization } from 'src/app/models/localization';

@Component({
  selector: 'app-tour-list',
  templateUrl: './tour-list.page.html',
  styleUrls: ['./tour-list.page.scss'],
})
export class TourListPage implements OnInit {
  user : User;
  initialDate;
  finalDate;
  tours : Tour[];
  departurePoint : Localization [] = [];
  places : Localization [] = [];
  kilometers = 1;

  constructor(private authService : FirebaseAuthService ,
             private databaseService : FirebaseDatabaseService,
             private menu: MenuController ) { }

  async ngOnInit() {
     await this.authService.getLoggedUser().then(res =>{
      this.user = res;
    }, err =>{
    });
  }

  ionViewWillEnter(){
    this.authService.getLoggedUser().then(res =>{
      this.user = res;
    }, err =>{
      this.user = null;
    });
   }

   basicSearch(){
    let initialDateFilter = (this.initialDate) ? this.initialDate.substring(0, 10) : null;
    let finalDateFilter = (this.finalDate)? this.finalDate.substring(0, 10) : null;
    let placesid = [];
    this.places.forEach(place => placesid.push(place.placeid));
    this.databaseService.getFilterTours(initialDateFilter, finalDateFilter, this.departurePoint[0], this.kilometers, placesid).then(tours => {
      this.tours = tours;
      this.menu.close("filter");
    })
   }

   showAllTours(){
     this.initialDate = "";
     this.finalDate = "";
     this.departurePoint = [];
     this.places = [];
     this.kilometers = 1;
    this.databaseService.getTours().then(tours => {
      this.tours = tours;
    })
   }

   openFirst() {
    this.menu.open('filter');
  }

}
