import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TourListPageRoutingModule } from './tour-list-routing.module';

import { TourListPage } from './tour-list.page';
import { TourGridComponent } from '../../Components/tour-grid/tour-grid.component';
import { LocationInputComponent } from '../../Components/location-input/location-input.component';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TourListPageRoutingModule,
    GoogleMapsModule
  ],
  declarations: [TourListPage,
    TourGridComponent,
    LocationInputComponent]
})
export class TourListPageModule {}
