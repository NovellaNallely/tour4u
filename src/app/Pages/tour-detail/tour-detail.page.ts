import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Tour } from 'src/app/models/tour';
import { User } from 'src/app/models/user';
import { ModalController, ViewWillEnter } from '@ionic/angular';
import { FirebaseDatabaseService } from '../../Services/firebase-database.service';
import { ToastController } from '@ionic/angular';
import { UserType } from 'src/app/models/user-type.enum';
import { Localization } from 'src/app/models/localization';
import { NavController } from '@ionic/angular';

import { Plugins } from '@capacitor/core';
const { Share } = Plugins;

@Component({
  selector: 'app-tour-detail',
  templateUrl: './tour-detail.page.html',
  styleUrls: ['./tour-detail.page.scss'],
})
export class TourDetailPage implements OnInit {

  @Input () tour : Tour ;
  @Input () tourists : User;
  @Input () user : User;

  zoom = 8;
  userType = UserType.Undefined;
  departurePoint : Localization [] = [];
  booked : boolean;
  
  constructor(public modalController: ModalController,
   private databaseService : FirebaseDatabaseService,
   private toastController: ToastController,
   private navCtrl: NavController) { }



  ngOnInit() {
    this.departurePoint[0] = {description : "Lugar de salida" , latitude: this.tour.latitude , longitude : this.tour.longitude}
  
    if(this.user.tours.indexOf(this.tour.id) >-1 ){
      this.booked = true;
    }
  }


  buy(){
    this.databaseService.buyTour(this.tour.id , this.user).then(res => {
      this.toast("Felicidades, has contratado un nuevo tour!!" , "");
    }, err => {
      this.toast("Error" , err.message);
      }) ;
  }

  dismiss(){
    this.navCtrl.pop();
    this.modalController.dismiss({
      'dismissed': true,
    });
  }

  async toast(title : string , message : string){
    const toast = await this.toastController.create({
      header: title,
      message: message,
      position: 'top',
      duration : 4000,
    });
    toast.present();
  }
}
