import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TourDetailPageRoutingModule } from './tour-detail-routing.module';
import { LocationInputComponent } from '../../Components/location-input/location-input.component';
import { TourDetailPage } from './tour-detail.page';
import { GoogleMapsModule } from '@angular/google-maps';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TourDetailPageRoutingModule,
    GoogleMapsModule
  ],
  declarations: [TourDetailPage,
    LocationInputComponent]
})
export class TourDetailPageModule {}
