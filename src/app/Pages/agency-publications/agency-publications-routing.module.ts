import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgencyPublicationsPage } from './agency-publications.page';

const routes: Routes = [
  {
    path: '',
    component: AgencyPublicationsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgencyPublicationsPageRoutingModule {}
