import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgencyPublicationsPageRoutingModule } from './agency-publications-routing.module';
import { TourGridComponent } from '../../Components/tour-grid/tour-grid.component';
import { AgencyPublicationsPage } from './agency-publications.page';
import { ImageInputComponent } from '../../Components/image-input/image-input.component';
import { LocationInputComponent } from '../../Components/location-input/location-input.component';

import { TourDetailPage } from '../../Pages/tour-detail/tour-detail.page';
import { AgencyAddtourPage } from '../../Pages/agency-addtour/agency-addtour.page';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgencyPublicationsPageRoutingModule,
    ReactiveFormsModule,
    GoogleMapsModule
  ],
  declarations: [AgencyPublicationsPage,
    TourGridComponent,
    ImageInputComponent,
    LocationInputComponent,
    TourDetailPage,
    AgencyAddtourPage
  ]
})
export class AgencyPublicationsPageModule {}
