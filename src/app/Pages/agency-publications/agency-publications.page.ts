import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'; 
import { Tour } from 'src/app/models/tour';
import { FirebaseAuthService } from '../../Services/firebase-auth.service';
import { User } from 'src/app/models/user';
import { UserType } from 'src/app/models/user-type.enum';
import { ModalController, ViewWillEnter } from '@ionic/angular';
import { AgencyAddtourPage } from '../agency-addtour/agency-addtour.page';
 
@Component({
  selector: 'app-agency-publications',
  templateUrl: './agency-publications.page.html',
  styleUrls: ['./agency-publications.page.scss'],
})
export class AgencyPublicationsPage implements OnInit , ViewWillEnter {
  user : User;
  type : UserType = 2;
  userID ;

  constructor(private route : Router , 
              private authService : FirebaseAuthService,
              public modalController: ModalController) {
               
  
 }

 ionViewWillEnter(){
  this.authService.getLoggedUser().then(res =>{
    this.user = res;
    this.type = res.type;
    this.userID = res.id;
  }, err =>{
    this.type = UserType.Undefined;
    this.user = null;
    this.userID =  "";
  });
 }
  ngOnInit() {
    
  }
  
  async addTour() {
    const modal = await this.modalController.create({
      component: AgencyAddtourPage,
      cssClass: 'my-custom-class',
      componentProps: {userID : this.user.id , mode : "add"}
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if(data['reload'] == true){
      location.reload();
    }
  }

}
