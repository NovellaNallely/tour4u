import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgencyPublicationsPage } from './agency-publications.page';

describe('AgencyPublicationsPage', () => {
  let component: AgencyPublicationsPage;
  let fixture: ComponentFixture<AgencyPublicationsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyPublicationsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgencyPublicationsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
