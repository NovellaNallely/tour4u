import { Component, OnInit, Input } from '@angular/core';
import { FormControl , FormGroup , Validators } from '@angular/forms';
import { SafeResourceUrl } from '@angular/platform-browser';
import { Tour } from 'src/app/models/tour';
import { FirebaseStorageService } from '../../Services/firebase-storage.service';
import { FirebaseDatabaseService } from '../../Services/firebase-database.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import geohash  from "ngeohash";
import * as firebase from 'firebase';
import { Localization } from 'src/app/models/localization';


@Component({
  selector: 'app-agency-addtour',
  templateUrl: './agency-addtour.page.html',
  styleUrls: ['./agency-addtour.page.scss'],
})
export class AgencyAddtourPage implements OnInit {
  @Input () mode;
  @Input () userID;
  @Input () tour : Tour ;
  @Input() tourID : string;

  tourValidation : FormGroup;
  submitAttempt : boolean;
  title : string;
  shortDescription : string;
  longDescription : string;
  images: SafeResourceUrl [] = [];
  price : number;
  interestingPoints = [];
  departurePoint : Localization [] = [];
  photos = [];
  initialDate  ;
  finalDate  ;

  constructor(private databaseService : FirebaseDatabaseService , 
              private storage : FirebaseStorageService ,
              private route: ActivatedRoute ,
              private toastController: ToastController,
              private modalController: ModalController) { }

  ngOnInit() {

    this.tourValidation = new FormGroup({
      title: new FormControl('', [Validators.required , Validators.minLength(20) , Validators.maxLength(100)]),
      shortDescription: new FormControl('', [Validators.required , Validators.minLength(50) , Validators.maxLength(200)]),
      longDescription: new FormControl('', [Validators.required]),
      price: new FormControl('', [Validators.required , Validators.pattern("^[0-9]*$")]),
      initialDate: new FormControl('', [Validators.required] ),
      finalDate : new FormControl ('', [Validators.required])
    });

    if(this.tourID != null){
      this.loadTour();
    }
  }

  processTour(){
    this.submitAttempt = true;
    if(!this.tourValidation.valid){
      return;
    }

    var promises = [];
    var urlImages = [];
    this.photos.forEach(photo => {
      if (String(photo).startsWith("https://firebasestorage"))
      { urlImages.push(photo);
        return;
      }
      var imagePath = "agency/"+ this.userID +"/"+ new Date().toISOString();
      promises.push(
        this.storage.saveImage(imagePath, photo).then(res => {
          urlImages.push(res);
        })
      );
    });

    let placesid = [];
    this.interestingPoints.forEach(place => placesid.push(place.placeid));


    Promise.all(promises).then(values => { 
      this.tour = {
        title : this.title,
        shortDescription : this.shortDescription,
        longDescription : this.longDescription,
        price : this.price,
        interestingPoints : this.interestingPoints,
        placesID : placesid,
        photos : urlImages,
        initialDate : this.initialDate,
        finalDate : this.finalDate,
        userID : this.userID,
        iDateSearch : this.initialDate.substring(0, 10),
        fDateSearch : this.finalDate.substring(0, 10),
      };

      if(this.departurePoint[0]){
        this.tour.latitude = this.departurePoint[0].latitude;
        this.tour.longitude = this.departurePoint[0].longitude;
        this.tour.geohash = geohash.encode(this.departurePoint[0].latitude, this.departurePoint[0].longitude)
      }
      
      switch(this.mode){
        case "add":
          this.databaseService.addTour(this.tour).then(res => {
            this.toast("Tour creado correctamente" , "");
            this.dismiss(true);
          }, err => {
            this.toast("Error" , err.message);
            }) ;
          break;
        case "edit": 
          this.tour.id = this.tourID;
          this.databaseService.updateTour(this.tour).then(res => {
            this.toast("Tour actualizado" , "");
            this.dismiss(true);
          }, err => {
              this.toast("Error " ,err.message);
            }) ;
          break;  
      }

    });
  }

  loadTour(){
    this.databaseService.getTourByID(this.tourID).then(tour => {
      this.tour = tour;
      this.userID = this.tour.userID;
      this.title = this.tour.title;
      this.shortDescription = this.tour.shortDescription;
      this.longDescription = this.tour.longDescription;
      this.price = this.tour.price;
      this.photos = this.tour.photos;
      this.interestingPoints = this.tour.interestingPoints;
      this.initialDate = this.tour.initialDate;
      this.finalDate = this.tour.finalDate;

      if(tour.latitude ){
        this.departurePoint[0] = {description : "Lugar de salida" , latitude: tour.latitude , longitude : tour.longitude}
      }
      });
  }

  async toast(title : string , message : string){
    const toast = await this.toastController.create({
      header: title,
      message: message,
      position: 'top',
      duration : 4000,
    });
    toast.present();
  }

  dismiss(reload : boolean) {
    this.modalController.dismiss({
      'dismissed': true,
      'reload' : reload
    });
  }
}
