import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AgencyAddtourPage } from './agency-addtour.page';

describe('AgencyAddtourPage', () => {
  let component: AgencyAddtourPage;
  let fixture: ComponentFixture<AgencyAddtourPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgencyAddtourPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgencyAddtourPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
