import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgencyAddtourPageRoutingModule } from './agency-addtour-routing.module';

import { AgencyAddtourPage } from './agency-addtour.page';
import { ImageInputComponent } from '../../Components/image-input/image-input.component';
import { LocationInputComponent } from '../../Components/location-input/location-input.component';
import { GoogleMapsModule } from '@angular/google-maps';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgencyAddtourPageRoutingModule,
    ReactiveFormsModule,
    GoogleMapsModule
  ],
  declarations: [AgencyAddtourPage,
    ImageInputComponent,
    LocationInputComponent
  ]
})
export class AgencyAddtourPageModule {}
