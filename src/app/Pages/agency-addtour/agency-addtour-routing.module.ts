import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgencyAddtourPage } from './agency-addtour.page';

const routes: Routes = [
  {
    path: '',
    component: AgencyAddtourPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgencyAddtourPageRoutingModule {}
