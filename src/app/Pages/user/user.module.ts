import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { UserPageRoutingModule } from './user-routing.module';
import { SignupFormComponent } from '../../Components/signup-form/signup-form.component'
import { UserSetupFormComponent } from '../../Components/user-setup-form/user-setup-form.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageInputComponent } from '../../Components/image-input/image-input.component';
import { UserPage } from './user.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [UserPage,
    SignupFormComponent,
    UserSetupFormComponent,
    ImageInputComponent
  ]
})
export class UserPageModule {}
