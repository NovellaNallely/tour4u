import { Component, OnInit } from '@angular/core';
import { FirebaseAuthService } from '../../Services/firebase-auth.service';
import { ViewWillEnter } from '@ionic/angular';

@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit ,  ViewWillEnter  {
  public user : boolean = false ;

  constructor(private authService : FirebaseAuthService ) { 

  }

  ngOnInit() {

  }

  async ionViewWillEnter(){
    await this.validate();
  }

  async validate(){
    await this.authService.getLoggedUser().then(res =>{
      this.user = true;
    }, err =>{
      this.user = false;
    });
  }

}
