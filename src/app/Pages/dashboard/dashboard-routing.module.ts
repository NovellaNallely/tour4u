import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPage } from './dashboard.page';

/*const routes: Routes = [
  {
    path: '',
    component: DashboardPage
  }
];*/

const routes: Routes = [
  {
    path: 'tabs',
    component: DashboardPage,
    children: [
      {
        path: 'tours',
        children: [
          {
            path: '',
            loadChildren: ()=> import('../tour-list/tour-list.module').then( m => m.TourListPageModule)
          }
        ]
      },
      {
        path: 'mine',
        children: [
          {
            path: '',
            loadChildren: ()=> import('../agency-publications/agency-publications.module').then( m => m.AgencyPublicationsPageModule)
          }
        ]
      },
      {
        path: 'profile',
        children: [
          {
            path: '',
            loadChildren: ()=> import('../user/user.module').then( m => m.UserPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/tours',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/dashboard/tabs/tours',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardPageRoutingModule {}
