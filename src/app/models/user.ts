import { UserType } from './user-type.enum';
import { Tour } from './tour';

export interface User {
    name? : string;
    lastname? : string;
    birthDay?;
    email : string;
    type : UserType;
    id? : string;
    profilePicture?;
    urlPage? : string;
    description? : string;
    localization? ;
    tours? : string [];
    tourists? : User [];
}
