import { GeolocationPosition } from '@capacitor/core';
import * as firebase from 'firebase';

export interface Localization {
    description : string;
    latitude : number;
    longitude : number;
    geohash? : number;
    geopoint? : firebase.firestore.GeoPoint;
    placeid? : string;
}
