export interface Tour {
    id? : string;
    title : string;
    shortDescription : string;
    longDescription? : string;
    price : number;
    interestingPoints? : any[] ;
    photos : any[];
    initialDate : Date;
    iDateSearch;
    fDateSearch;
    finalDate : Date;
    userID : string;
    latitude?: number;
    longitude? : number;
    geohash? : number;
    placesID? ;
}
