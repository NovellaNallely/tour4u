import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { UserType } from '../models/user-type.enum';

@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService extends StorageService {
  saveImage(path: string , base64 : any) {
    var data = this.dataURItoBlob(base64);
    return new Promise<string>((resolve, reject) => {
      var ref = this.storage.ref(path);
      var uploadTask = ref.put(data);
      uploadTask.then(
        res => {resolve(res.ref.getDownloadURL());},
        err => {reject(err);})
        .catch((error) => {
          reject(error)
        })
    } )
  }

  constructor(private storage: AngularFireStorage) {
    super();
   }

   dataURItoBlob(dataURI) {
    var binary = atob(dataURI.split(',')[1]);
    var array = [];
  for (var i = 0; i < binary.length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], {
    type: 'image/jpg'
  });
}
}
