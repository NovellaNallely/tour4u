import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Tour } from '../models/tour';
import { Localization } from '../models/localization';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export abstract class DatabaseService {
  abstract createUser(user: User): Promise<User>;
  abstract getUser(id : string ) : Promise<User>;
  abstract addTour( tour : Tour) : Promise <Tour>;
  abstract updateTour( tour : Tour) : Promise <Tour>;
  abstract getToursByAgency(userID : string) : Promise <Tour[]>;
  abstract getToursByTourist(userID : string) : Promise <Tour[]>;
  abstract getTourByID(id: string) : Promise <Tour>;
  abstract getFilterTours(initialDate, finalDate , localization : Localization , km : number, places : string []) : Promise <Tour[]>;
  abstract getTours() : Promise <Tour []>;
  abstract getUsersByTour(tourID : string) : Promise <User []>;
  abstract buyTour(tourID : string, user : User) : Promise <void>;
  constructor() { }
}

