import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { User } from '../models/user';
import { AngularFirestore } from '@angular/fire/firestore';
import { Tour } from '../models/tour';
import * as firebase from 'firebase';
import geohash from "ngeohash";
import { Localization } from '../models/localization';
import { UserType } from '../models/user-type.enum';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDatabaseService extends DatabaseService{

   /*getUser(userID : string) : Promise <User> {
    var userRef = this.firestore.firestore.collection("users");
    userRef.doc(userID).get().then(function (querySnapshot){
      var user = <User>querySnapshot.data();
      return user;
    });
  }*/

  constructor(private firestore: AngularFirestore) {
    super();
  }

  getUser(userID: string): Promise<User> {
    return new Promise <User>((resolve , reject) =>{
      var userRef = this.firestore.firestore.collection("users");
      userRef.doc(userID).get().then(function (querySnapshot){
        var user = <User>querySnapshot.data();
        resolve(user);
      })
    });
  }

  createUser(user: User): Promise<User> {
    return new Promise<any>((resolve, reject) => {
      //this.firestore.collection("users").add(user)
      this.firestore.collection("users").doc(user.id).set(user).then(
        res => {resolve(res);},
        err => {reject(err);})
        .catch((error) => {
          reject(error)
        })
    } )
  }

  addTour(tour : Tour): Promise<Tour> {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection("tours").add(tour).then(
        res => {
          resolve(res);},
        err => {reject(err);}
        )}
        )
  }

  updateTour(tour : Tour): Promise<Tour> {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection("tours").doc(tour.id).set(tour).then(
        res => {
          resolve(res);},
        err => {reject(err);}
        )}
        )
  }

  getToursByAgency(userID: string): Promise<Tour[]> {
    return new Promise <Tour[]>((resolve , reject) =>{
      var toursCollection = this.firestore.firestore.collection("tours");
      toursCollection.where("userID" , "==" , userID).get().then(function (querySnapshot){
        var tours = querySnapshot.docs.map((dataItem : firebase.firestore.QueryDocumentSnapshot) => {
          var t = <Tour>dataItem.data();
          t.id = dataItem.id;
          return t;
        });
        resolve(tours);
      })
    });
  }

  getToursByTourist(userID: string): Promise<Tour[]> {
    return new Promise <Tour[]>(async (resolve , reject) =>{
      var firestore = this.firestore.firestore;
      var userRef = this.firestore.firestore.collection("users");
      userRef.doc(userID).get().then(querySnapshot=>{
        var resTours : Tour[] = [];
        var user = <User>querySnapshot.data();
        var promises = [];
        user.tours.forEach(tourID => {
          var tourRef = firestore.collection("tours");
          promises.push(
            tourRef.doc(tourID).get().then(res =>{
              var tour = <Tour>res.data();
              resTours.push(tour);
            }
          ));
          Promise.all(promises).then(values => { 
            //tours = <Tour[]>values;
            console.log("values");
            console.log(values);
            resolve(resTours);
          })
        });
      })


      /*var toursCollection = this.firestore.firestore.collection("users");
      toursCollection.where("tourist" , "array-contains" , userID).get().then(function (querySnapshot){
        var tours = querySnapshot.docs.map((dataItem : firebase.firestore.QueryDocumentSnapshot) => {
          var t = <Tour>dataItem.data();
          t.id = dataItem.id;
          return t;
        });
        resolve(tours);
      })*/
    });
  }

  getTours(): Promise<Tour[]> {
    return new Promise <Tour[]>((resolve , reject) =>{
      var toursCollection = this.firestore.firestore.collection("tours");
      toursCollection.get().then(function (querySnapshot){
        var tours = querySnapshot.docs.map((dataItem : firebase.firestore.QueryDocumentSnapshot) => {
          var t = <Tour>dataItem.data();
          t.id = dataItem.id;
          return t;
        });
        resolve(tours);
      })
    });
  }

  getTourByID(tourID: string): Promise<Tour> {
    return new Promise <Tour>((resolve , reject) =>{
      var toursCollection = this.firestore.firestore.collection("tours");
      toursCollection.doc(tourID).get().then(function (querySnapshot){
        var tour = <Tour>querySnapshot.data();
        tour.id = tourID;
        resolve(tour);
      })
    });
  }

  async deleteTour(tourID : string) : Promise<any>{
   var users = await this.getUsersByTour(tourID);
    if(users){
        return new Promise((resolve , reject) =>{
         reject('No se pueden eliminar tours contratados');
      })
  }

    return new Promise <any>((resolve , reject) =>{
          var toursCollection = this.firestore.firestore.collection("tours");
          toursCollection.doc(tourID).delete().then(function (querySnapshot){
          resolve(null);
        })
    });
  }

  buyTour(tourID : string , user : User): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.firestore.collection("tours").doc(tourID).collection("users").doc(user.id).set({name : user.name , lastname: user.lastname, email :user.email}).then(
        res => {
          this.firestore.collection("users").doc(user.id).update({
            tours: firebase.firestore.FieldValue.arrayUnion(tourID)
          }).then(res =>{
            resolve(res);}
          ,err =>{reject(err);})
         ,
        err => {reject(err);}
     });
    });
  }

  getFilterTours(initialDate, finalDate , departurePoint : Localization , km : number , places : string [] ) : Promise<Tour []> {
    return new Promise <Tour[]>((resolve , reject) =>{
      var toursCollection = this.firestore.firestore.collection("tours").orderBy("geohash");
      if (departurePoint){
        let range = this.getGeohashRange(departurePoint.latitude, departurePoint.longitude, km);
        toursCollection = toursCollection.where("geohash", ">=", range.lower)
        .where("geohash", "<=", range.upper);
      }

      if(places.length > 0){
          toursCollection = toursCollection.where("placesID", "array-contains-any" , places);
      }
      if (initialDate) toursCollection = toursCollection.where("iDateSearch", "==", initialDate);
      if (finalDate) toursCollection = toursCollection.where("fDateSearch" , "==" , finalDate);

      var finaltours = [];
      toursCollection.get().then(function (querySnapshot){
        var tours = querySnapshot.docs.map((dataItem : firebase.firestore.QueryDocumentSnapshot) => {
          var t = <Tour>dataItem.data();
          t.id = dataItem.id;

          if(places.every(function(val) { return t.placesID.indexOf(val) >= 0; })){
            finaltours.push(t);
          }

          return t; 
        });
        resolve(finaltours);
      })
    });
  }

  getUsersByTour(tourID): Promise<User[]> {
    return new Promise <User[]>((resolve , reject) =>{
      var tourUserCollection = this.firestore.firestore.collection("tours").doc(tourID).collection("users");
      tourUserCollection.get().then(function (querySnapshot){
        var users = querySnapshot.docs.map((dataItem : firebase.firestore.QueryDocumentSnapshot) => {
          var u = <User>dataItem.data();
          u.type = UserType.Tourist;
          return u;
        });
        resolve(users);
      })
    });
  }

  getGeohashRange (latitude: number, longitude: number, distance: number) {

    const lat = 0.009005401980432; // degrees latitude per KM
    const lon = 0.011297686120906; // degrees longitude per KM
  
    const lowerLat = latitude - lat * distance;
    const lowerLon = longitude - lon * distance;
  
    const upperLat = latitude + lat * distance;
    const upperLon = longitude + lon * distance;
  
    const lower = geohash.encode(lowerLat, lowerLon);
    const upper = geohash.encode(upperLat, upperLon);
  
    return {
      lower,
      upper
    };
  };

}
