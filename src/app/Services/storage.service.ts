import { Injectable } from '@angular/core';
import { UserType } from '../models/user-type.enum';

@Injectable({
  providedIn: 'root'
})
export abstract class StorageService {

  abstract saveImage(nameImage: String  , data : any,  usertype : UserType);
  constructor() { }
}
