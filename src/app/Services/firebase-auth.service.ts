import { Injectable, ErrorHandler } from '@angular/core';
import * as firebase from 'firebase';
import { User } from '../models/user';
import { FirebaseDatabaseService } from './firebase-database.service';
import { UserType } from '../models/user-type.enum';

@Injectable({
  providedIn: 'root'
})
export class FirebaseAuthService {

  private user : User;
  constructor(public dataService : FirebaseDatabaseService) { }

  registerUser(email , password){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(
        res => {
          var initialUser : User = {
            email : email,
            type : UserType.Undefined,
            id : res.user.uid
          };
          this.dataService.createUser(initialUser).then(res =>{
            resolve(res);
          });
        },
        err => {reject(err);})
        .catch((error) => {
          reject(error)
        })
    } )
  }

  loginUser(email , password){
    firebase.auth().languageCode = 'es';
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
    return new Promise<any>((resolve, reject) => {
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        async res => {
          this.addSessionListener();
          resolve(res);
        },
        err => {reject(err);})
        .catch((error) => {
          reject(error)
        })
    } )
  }

  resetPassword(email){
    return new Promise<any>((resolve, reject) => {
      firebase.auth().sendPasswordResetEmail(email)
      .then(
        res => resolve(res),
        err => {reject(err);})
        .catch((error) => {
          reject(error)
        })
    } )
  }

  async getLoggedUser () : Promise  <User>{
    return new Promise <User>(async (resolve, reject) =>{
      //let userAuth = firebase.auth().currentUser;
      const unsubscribe = firebase.auth().onAuthStateChanged(user => {
        unsubscribe();
        if(user){
          /*if(this.user){
            resolve(this.user);
          }*/
          this.dataService.getUser(user.uid).then(res => {
            this.user = res;
            resolve(this.user);
          }, reject);
        }
        else{
          reject();
        }
        
      });
    });
  }

  closeSession(){
    this.user = null;
    firebase.auth().signOut();
  }

  addSessionListener() {
    firebase.auth().onAuthStateChanged((user)=> {
      if (user != null) {
          this.dataService.getUser(user.uid).then(res => {
          this.user = res;
        }, err => {
       }) ;
      } else {
        this.user = null;
      }
    });
  }

}
