import { Component, OnInit, Input, Output , EventEmitter  } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ViewChild, ElementRef } from '@angular/core';
import { Plugins ,CameraResultType, CameraSource, Capacitor } from '@capacitor/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { FirebaseStorageService } from '../../Services/firebase-storage.service';

const { Camera } = Plugins;

@Component({
  selector: 'app-image-input',
  templateUrl: './image-input.component.html',
  styleUrls: ['./image-input.component.scss'],
})
export class ImageInputComponent implements OnInit {
  @Input() multiple : boolean;
  @Input() photo: SafeResourceUrl [] = [];
  @Output() setImage: EventEmitter<any> = new EventEmitter();
  @Input() title : string;
  @Input() defaultIcon : string;
  

  file: File;
  @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;
  isDesktop: boolean;

  constructor(private platform: Platform, private sanitizer: DomSanitizer, private storageService : FirebaseStorageService) { 
  }

  ngOnInit() {
    if ((this.platform.is('mobile') && this.platform.is('hybrid')) || this.platform.is('desktop')) {
      this.isDesktop = true;
    }
  }

  public async getPicture(type: string) {
    if (!Capacitor.isPluginAvailable('Camera') || (this.isDesktop && type === 'gallery')) {
      this.filePickerRef.nativeElement.click();
      return;
    }

    const image = await Camera.getPhoto({
      quality: 100,
      width: 400,
      allowEditing: false,
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera
    });

    if(this.multiple){
      var resource = 'data:image/jpeg;base64,' + image.base64String;
      this.photo.push(resource);
      //this.photo.push(image.base64String);
      //this.photo.push(this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl)));
    }else{
      this.photo[0] = 'data:image/jpeg;base64,' + image.base64String;
      //this.photo[0] = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl));
    }
    this.notifyChange();
  }

  onFileChoose(event: Event) {
    const files = (event.target as HTMLInputElement).files;
    const pattern = /image-*/;

    for (var i = 0; i < files.length; i++) {
      const reader = new FileReader();
      if (!files[i].type.match(pattern)) {
        return;
      }

      reader.onload = () => {
        
        if(this.multiple){
          this.photo.push(reader.result.toString());
          this.notifyChange();
        } else {
          this.photo[0] = reader.result.toString();
          this.notifyChange();
        }
      };
      reader.readAsDataURL(files[i]);
    }
   
  }

  notifyChange(){
    this.setImage.emit(this.photo);
  }

  deleteImage(index){
    this.photo.splice(index, 1);
    this.notifyChange();
  }

}
