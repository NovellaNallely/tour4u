import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { FormControl , FormGroup , Validators } from '@angular/forms';
import { FirebaseAuthService } from '../../Services/firebase-auth.service';
import { User } from '../../models/user'
import { UserType } from 'src/app/models/user-type.enum';
import { FirebaseDatabaseService } from '../../Services/firebase-database.service';
import { FirebaseStorageService } from '../../Services/firebase-storage.service';
import { SafeResourceUrl } from '@angular/platform-browser';
import { ToastController } from '@ionic/angular';



@Component({
  selector: 'app-user-setup-form',
  templateUrl: './user-setup-form.component.html',
  styleUrls: ['./user-setup-form.component.scss'],
})

export class UserSetupFormComponent implements OnInit { 
  @Output() usserLogged: EventEmitter<any> = new EventEmitter();

  user : User;
  agencyValidation: FormGroup;
  userValidation: FormGroup;
  submitAttempt : boolean;

  userTypeSelected : UserType;
  agencyName : string = "";
  agencyURL : string = "";
  agencyDescription : string = "";

  touristName : string = "";
  touristLastname : string = "";
  touristBirthday;

  images: SafeResourceUrl [] = [];

  constructor( private databaseService : FirebaseDatabaseService , 
               private storage : FirebaseStorageService , 
               private authService : FirebaseAuthService,
               private toastController: ToastController) { }

  ngOnInit() {
    this.authService.getLoggedUser().then(res =>{
      this.user = res;
      this.setActiveUser();
    }, err =>{
      
    });

    this.agencyValidation = new FormGroup({
      agencyName: new FormControl('', [Validators.required]),
      agencyURL: new FormControl('', []),
      agencyDescription: new FormControl('', [Validators.required])
    });

    this.userValidation = new FormGroup({
      touristName: new FormControl('', [Validators.required]),
      touristLastname: new FormControl('', [Validators.required]),
      touristBirthday: new FormControl('', [Validators.required])
    });
    
  }

  async setupAgency(){
    console.log("setup agency");
    this.submitAttempt = true;
    if(!this.agencyValidation.valid){
      return;
    }

    this.user.type = UserType.Agency;
    this.user.name = this.agencyName;
    this.user.description = this.agencyDescription;
    this.user.urlPage = this.agencyURL; 
    

    if(this.images.length > 0 && !String(this.images[0]).startsWith("https://firebasestorage")){
      var imagePath = this.user.id+ new Date().toISOString();
      this.user.profilePicture = await this.storage.saveImage(imagePath, this.images[0]);
    }

    this.databaseService.createUser(this.user).then((res=>{
      this.toast("","Cambios de la agencia guardados");
    }),(err=>{
      this.toast("Error ", err);
    }));
  }

  async setupTourist(){
    console.log("setup user");
    this.submitAttempt = true;
    if(!this.userValidation.valid){
      this.toast("","Cambios del usuario guardados");
      return;
    }

    this.user.type = UserType.Tourist;
    this.user.name = this.touristName;
    this.user.lastname = this.touristLastname;
    this.user.birthDay = this.touristBirthday;
    
    if(this.images.length > 0){
      var imagePath = this.user.id+ new Date().toISOString();
      this.user.profilePicture = await this.storage.saveImage(imagePath, this.images[0]);
    }

    this.databaseService.createUser(this.user).then((res=>{
      this.toast("","Usuario guardado");
    }),(err=>{
      this.toast("Error",err);
    }));
  }

  setActiveUser(){
    switch(this.user.type){
      case UserType.Agency:
        this.userTypeSelected = UserType.Agency;
        this.agencyDescription = this.user.description;
        this.agencyName = this.user.name;
        this.agencyURL = this.user.urlPage;
        break;
      case UserType.Tourist:
        this.userTypeSelected = UserType.Tourist;
        this.touristName = this.user.name;
        this.touristLastname = this.user.lastname;
        this.touristBirthday = this.user.birthDay;
        break;  
    }

   if (this.user.profilePicture ) {this.images[0] = this.user.profilePicture;}
   
  }

  setImage(image){
    this.images = image;
  }

  closeSession(){
    this.authService.closeSession();
    this.usserLogged.emit();
  }

  async toast(title : string , message : string){
    const toast = await this.toastController.create({
      header: title,
      message: message,
      position: 'top',
      duration : 4000,
    });
    toast.present();
  }

}
