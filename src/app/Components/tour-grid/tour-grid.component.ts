import { Component, OnInit, Input } from '@angular/core';
import { Tour } from 'src/app/models/tour';
import { FirebaseDatabaseService } from '../../Services/firebase-database.service';
import { AgencyAddtourPage } from '../../Pages/agency-addtour/agency-addtour.page';
import { TourDetailPage } from '../../Pages/tour-detail/tour-detail.page';
import { User } from '../../models/user';
import { AlertController} from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tour-grid',
  templateUrl: './tour-grid.component.html',
  styleUrls: ['./tour-grid.component.scss'],
})
export class TourGridComponent implements OnInit {
  @Input() displayType : string;
  @Input() userID : string;
  @Input() user : User;
  @Input () mode;
  @Input () tours;

  constructor(private databaseService : FirebaseDatabaseService , 
              private alertController: AlertController,
              public modalController: ModalController,
              private toastController: ToastController) { }

  ngOnInit() {
    switch(this.displayType){
      case "agency" : this.displayAgency();
      break;
      case "catalog" : this.displayCatalog();
      break;   
      case "tourist" :  this.displayTourist();
      break; 
    }   
  }

  displayCatalog(){
    this.databaseService.getTours().then(tours => {
      this.tours = tours;
    })
  }

  displayAgency(){
    this.databaseService.getToursByAgency(this.userID).then(tours => {
      this.tours = tours;
    })
  }

  displayTourist(){
    this.databaseService.getToursByTourist(this.userID).then(tours => {
      this.tours = tours;
    })
  }

  
  async displayTour(tourID , mode) {
   
    var modal = await this.modalController.create({
      component: AgencyAddtourPage,
      componentProps: {tourID : tourID , mode : mode}
    });
    await modal.present();
    const { data } = await modal.onWillDismiss();
    if(data['reload'] == true){
      this.ngOnInit();
    }
  }

  async viewTour(tour : Tour , mode) {
    let tourists;
    if(mode == 'agency'){
       await this.databaseService.getUsersByTour(tour.id).then(tours => {
        tourists = tours;
      })
    } 

    var modal = await this.modalController.create({
      component: TourDetailPage,
      componentProps: {tour : tour , user : this.user , mode : mode , tourists : tourists }
    });
    await modal.present();
  }

  async deleteTour(tour : Tour){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Eliminar',
      message: '¿Estas seguro que deseas <strong>eliminar</strong> este tour?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Si',
          handler: async () => {
          this.databaseService.deleteTour(tour.id).then(async res => {
              this.toast("" ,"El tour se ha eliminado");
              this.ngOnInit();
            },(err=>{
              console.log("Error capturado");
              this.toast("Error", err);
            }))
          }
        }
      ]
    });

    await alert.present();

  }

  async toast(title : string , message : string){
    const toast = await this.toastController.create({
      header: title,
      message: message,
      position: 'top',
      duration : 4000,
    });
    toast.present();
  }

}
