import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl , FormGroup , Validators } from '@angular/forms';
import { FirebaseAuthService } from '../../Services/firebase-auth.service';
import { AlertController } from '@ionic/angular';
import {Router} from '@angular/router'; 


@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
})
export class SignupFormComponent implements OnInit {
  @Output() usserLogged: EventEmitter<any> = new EventEmitter();

  userValidation: FormGroup;
  errorMsg : String;
  submitAttempt : boolean;
  public email : String ;
  public password : String ;
  public hasAccount : Boolean;

  constructor(private authService : FirebaseAuthService , private alertCtrl: AlertController , private router : Router) { }

  ngOnInit() {
    this.userValidation = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
      hasAccount: new FormControl('',)
    });
  }

  private userActionForm(){
    this.submitAttempt = true;
    if (!this.userValidation.valid) {
      return;
    }
    if(!this.hasAccount){
      this.authService.registerUser(this.email , this.password).then(res => {
        this.errorMsg = "";
        this.usserLogged.emit();
      }, err => {
          this.errorMsg = err.message;
        }) ;
      }
    else{
      this.authService.loginUser(this.email , this.password).then(res => {
        this.errorMsg = "";
        this.usserLogged.emit();
      }, err => {
          this.errorMsg = err.message;
        }) ;
    }
  }

  async resetEmail(){
      const alert = await this.alertCtrl.create({
        header: 'Enviar correo para reestablecer contraseña',
        inputs: [
          {
            name: 'emailReset',
            type: 'email',
            placeholder: 'Correo electrónico'
          },
        ],
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            cssClass: 'secondary',
            handler: () => {
            }
          }, {
            text: 'Enviar',
            handler: (alertData) => {
              this.sendResetEmail(alertData.emailReset);
            }
          }
        ]
      });
      await alert.present();
  }

  sendResetEmail(email : string){
    this.authService.resetPassword(email).then(res => {
      this.errorMsg = "";
    }, err => {
        this.errorMsg = err.message;
      }) ;
  }

  notifyChange(){
    this.usserLogged.emit();
  }
 

}
