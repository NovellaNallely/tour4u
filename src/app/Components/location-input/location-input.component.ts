import { Component, OnInit, ViewChild, Input, Output , EventEmitter, AfterViewInit, ElementRef, OnDestroy, AfterContentChecked, AfterViewChecked } from '@angular/core';
import { Geolocation  } from '@capacitor/core';
import {  MapMarker, GoogleMap } from '@angular/google-maps';
import { Localization } from '../../models/localization'
import { ToastController, ViewDidEnter } from '@ionic/angular';
import * as firebase from 'firebase';


@Component({
  selector: 'app-location-input',
  templateUrl: './location-input.component.html',
  styleUrls: ['./location-input.component.scss'],
})
export class LocationInputComponent implements OnInit , AfterViewInit, OnDestroy   {

  @Input() adressType: string;
  @Input() max : number;
  @Input() multiple : boolean;
  @Input() markers : Localization [] = [];
  @Input() title : string;
  @Input() onlyCatalog : boolean;
  @Input() showMap : boolean;
  @Input() mode : string;
  @Input() idMap : string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext') addresstext: any;
  @ViewChild('mapita', { static: false }) mapita: ElementRef;
  map : google.maps.Map;

  latitude: number;
  longitude: number;
  center = (this.markers[0])? this.getLatLng( this.markers[0].latitude , this.markers[0].longitude ) :this.getLatLng( 19.39068 , -99.2836985 );//{lat: 19.39068, lng: -99.2836985};
  zoom: number = 9;
  loading : boolean;
  autocompleteInput: string;
  queryWait: boolean;

  options: google.maps.MapOptions = {
    maxZoom: 15,
    minZoom: 4,
    panControl : true
  }

  constructor(public toastController: ToastController) {
  }

  ionViewDidEnter(): void {
  }
  ngOnDestroy(): void {
    this.showMap = false;
    this.map = null;
  }


  ngAfterViewInit(): void {
    if(this.mode != 'view'){
      this.getPlaceAutocomplete();
    } 

    let mapOptions = {
      center: this.getLatLng(0,0),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
  }

  ngOnInit() {
  }

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
        {
            componentRestrictions: { country: 'MX' },
            types: ["establishment"]  // 'establishment' / 'address' / 'geocode'
        });
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        this.loading = true;
        this.invokeEvent(place);
    });
  }

  invokeEvent(place: any) {
    var loc = place['geometry']['location'];
    var lat = loc.lat();
    var lng = loc.lng();
    this.addMarker(lat , lng,place['name'], place['place_id']);
    this.center = loc;
    this.zoom = 15;
    this.notifyChange();
    this.autocompleteInput="";
    this.addresstext.nativeElement.focus();
  }

  async getLocation() {
    this.loading = true;
    const position = await Geolocation.getCurrentPosition();
    this.latitude = position.coords.latitude;
    this.longitude = position.coords.longitude;
    this.center = this.getLatLng(this.latitude , this.longitude);
    this.zoom = 15;
    this.addMarker(position.coords.latitude, position.coords.longitude,"Ubicación actual", "green");
  }

  async addMarker(lat : number , lng : number , title: string  , placeid? : string){ 
    var geopoint  = new firebase.firestore.GeoPoint (lat , lng);
    var location : Localization = {description : title, latitude : lat , longitude: lng , placeid : placeid} ;

    if(this.max && this.markers.length == this.max){
      const toast = await this.toastController.create({
        message: "Solo se pueden seleccionar " + this.max + " lugares",
        duration: 2000,
        position: 'top'
      });
      toast.present();
      return;
    }

    if(this.multiple){
      this.markers.push(location);
    }else{
      this.markers[0] = location;
    }
    this.notifyChange();
    this.loading = false;
  }

  mapAddMarker(event: google.maps.MouseEvent){
    if(this.onlyCatalog) return;
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    this.addMarker(lat, lng, "Ubicación", "Green" )
  }

  markerPositionChanged(marker : MapMarker , event: google.maps.MouseEvent){
    marker.position = event.latLng;
    this.notifyChange();
  }

  deleteMarker(index){
    this.markers.splice(index, 1);
    this.notifyChange();
  }

  notifyChange(){
    this.setAddress.emit(this.markers); 
  }

  getLatLng(lat , lon){
    return new google.maps.LatLng(lat, lon);
  }

}
